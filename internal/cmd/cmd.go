package cmd

import (
	"auto-block/internal/service/ufw"
	"context"
	"github.com/marcellowy/go-common/gogf/vlog"

	"github.com/gogf/gf/v2/os/gcmd"
)

var (
	Main = gcmd.Command{
		Name:  "main",
		Usage: "main",
		Brief: "start http server",
		Func: func(ctx context.Context, parser *gcmd.Parser) (err error) {
			//s := g.Server()
			//s.Group("/", func(group *ghttp.RouterGroup) {
			//	group.Middleware(ghttp.MiddlewareHandlerResponse)
			//	group.Bind(
			//		hello.NewV1(),
			//	)
			//})
			//s.Run()
			vlog.Info(ctx, "begin")
			ufw.Start(ctx)
			return nil
		},
	}
)
