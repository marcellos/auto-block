package main

import (
	_ "auto-block/internal/packed"

	"github.com/gogf/gf/v2/os/gctx"

	"auto-block/internal/cmd"
)

func main() {
	cmd.Main.Run(gctx.GetInitCtx())
}
